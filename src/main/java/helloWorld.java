import java.util.ResourceBundle;
import java.util.logging.Logger;

public class helloWorld {

    public static void main(String[] args) {
        Logger log = Logger.getLogger(helloWorld.class.getName());
        ResourceBundle resourceBundle = ResourceBundle.getBundle("Profile");
        String profileName = resourceBundle.getString("profileName");

        log.info("Hello Profile "+ profileName);

    }
}
